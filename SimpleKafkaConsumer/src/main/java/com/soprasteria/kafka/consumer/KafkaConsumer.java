package com.soprasteria.kafka.consumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.bson.Document;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * @author paoloscarpino
 *
 */
public class KafkaConsumer {

  private Consumer<String, String> consumer;
  private int minBatchSize;
  private boolean isStopped;

  private MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
  private MongoClient mongoClient = new MongoClient(connectionString);
  private MongoDatabase database = mongoClient.getDatabase("testdb");
  private MongoCollection<Document> collection = database.getCollection("mycollection");

  /**
   * 
   * @param groupId
   * @param servers
   * @param topics
   * @param minBatchSize
   */
  public KafkaConsumer(String groupId, String servers, String topics, int minBatchSize) {
    super();
    if (minBatchSize > 0)
      this.minBatchSize = minBatchSize;
    else
      this.minBatchSize = 1;

    createConsumer(groupId, servers, topics);
  }

  /**
   * 
   * @param groupId
   * @param servers
   * @param topics
   */
  private void createConsumer(final String groupId, final String servers, final String topics) {
    final Properties props = new Properties();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

    // Create the consumer using props.
    consumer = new org.apache.kafka.clients.consumer.KafkaConsumer<>(props);

    // Subscribe to the topic.
    consumer.subscribe(Collections.singletonList(topics));

    isStopped = false;
  }

  /**
   * 
   * @throws Exception
   */
  public void runConsumer() throws Exception {
    List<Document> buffer = new ArrayList<Document>();

    try {
      while (!isStopped) {
        final ConsumerRecords<String, String> consumerRecords = consumer.poll(100);

        for (TopicPartition partition : consumerRecords.partitions()) {
          List<ConsumerRecord<String, String>> partitionRecords =
              consumerRecords.records(partition);
          partitionRecords.forEach(record -> {
            String id = partition.topic() + "#" + partition.partition() + "#" + record.offset();
            Document doc = new Document("_id", id).append("value", record.value());
            buffer.add(doc);
          });
        }
        if (buffer.size() >= minBatchSize) {
          // Some operations - e.g. insertIntoDb(buffer);
          collection.insertMany(buffer);
          buffer.clear();
        }
        consumer.commitSync();
      }
    } finally {
      consumer.close();
    }
  }

  /**
   * 
   */
  public void stopConsumer() {
    mongoClient.close();
    isStopped = true;
  }
}
