package com.soprasteria.kafka.consumer;

import java.util.concurrent.CountDownLatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * java -jar
 * /Users/paoloscarpino/eclipse-workspace/SimpleKafkaConsumer/target/SimpleKafkaConsumer-1.0.0-
 * shaded.jar group_one 127.0.0.1:9092 test2 200
 */

/**
 * @author paoloscarpino
 *
 */
public class MainConsumer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MainConsumer.class);

  /**
   * 
   * @param args
   * @throws Exception
   */
  public static void main(String... args) throws Exception {
    
    String jarFileName = new java.io.File(
        MainConsumer.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();

    if (args.length == 4) {
      LOGGER.info("groupId: " + args[0]);
      LOGGER.info("servers: " + args[1]);
      LOGGER.info("topics: " + args[2]);
      LOGGER.info("batch size: " + args[3]);

      KafkaConsumer consumer =
          new KafkaConsumer(args[0], args[1], args[2], new Integer(args[3]).intValue());

      CountDownLatch latch = new CountDownLatch(1);

      Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
        @Override
        public void run() {
          LOGGER.info("Stopping Consumer \n");
          consumer.stopConsumer();
          latch.countDown();
        }
      });

      try {
        consumer.runConsumer();
      } catch (Exception e) {
        e.printStackTrace();
      }

    } else {
      LOGGER.error("Usage: java -jar {} <groupId> <servers> <topics> <BatchSize> \n", jarFileName);
      LOGGER.error(
          "\tE.g.: java -jar {} group_one 127.0.0.1:9092,127.0.0.1:9093,127.0.0.1:9094 test1,test2,test3 200 \n",
          jarFileName);
    }
  }
}
