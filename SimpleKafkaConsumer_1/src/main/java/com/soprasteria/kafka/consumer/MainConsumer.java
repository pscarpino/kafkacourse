package com.soprasteria.kafka.consumer;

import java.util.Collections;
import java.util.Properties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author paoloscarpino
 *
 */
public class MainConsumer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MainConsumer.class);

  /**
   * 
   * @param args
   * @throws Exception
   */
  public static void main(String... args) throws Exception {

    final Properties props = new Properties();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "test_1");
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

    // Create the consumer using props.
    Consumer<String, String> consumer = new KafkaConsumer<>(props);

    // Subscribe to the topic.
    consumer.subscribe(Collections.singletonList("TOPIC_TEST_2"));


    try {
      while (true) {
        final ConsumerRecords<String, String> consumerRecords = consumer.poll(100);

        for (ConsumerRecord<String, String> record : consumerRecords) {
          LOGGER.info(record.topic() + "#" + record.offset() + "#" + record.value());
        }

        consumer.commitSync();
      }
    } finally {
      consumer.close();
    }

  }
}
