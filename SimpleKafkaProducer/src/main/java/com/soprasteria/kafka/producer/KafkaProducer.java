package com.soprasteria.kafka.producer;

import java.util.Properties;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;


/**
 * KafkaProducer.
 * 
 * @author paoloscarpino
 *
 */
public class KafkaProducer {

  private Producer<String, String> producer;

  /**
   * createProducer.
   * 
   * @param servers brokers list
   */
  public void createProducer(final String servers) {
    Properties props = new Properties();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
    props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    producer = new org.apache.kafka.clients.producer.KafkaProducer<>(props);
  }

  /**
   * sendMessage.
   * 
   * @param msg message to send
   * @param topic topic list
   * @throws Exception when this exceptional condition happens
   */
  public void sendMessage(final String msg, final String topic, final Callback callBack)
      throws Exception {
    long time = System.currentTimeMillis();

    try {
      final ProducerRecord<String, String> record = new ProducerRecord<>(topic, "" + time, msg);

      producer.send(record, callBack);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * flush.
   */
  public void flush() {
    producer.flush();
  }

  /**
   * closeProducerSession.
   * 
   * @throws Exception when this exceptional condition happens
   */
  public void closeProducerSession() throws Exception {
    producer.close();
  }


}
