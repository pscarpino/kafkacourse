package com.soprasteria.kafka.producer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * java -jar
 * /Users/paoloscarpino/eclipse-workspace/SimpleKafkaProducer/target/SimpleKafkaProducer-1.0.0-
 * shaded.jar 127.0.0.1:9092 test1 10 5000
 * /Users/paoloscarpino/Downloads/prova_spark/yahoo_stocks.csv
 */


/**
 * MainProducer.
 * 
 * @author paoloscarpino
 *
 */
public class MainProducer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MainProducer.class);

  private static List<List<String>> partitions;
  private static int partiotionElaboratedPosition = 0;
  private static KafkaProducer producer = new KafkaProducer();

  public static class MyCallback implements Callback {
    public boolean success = true;

    @Override
    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
      if (e != null) {
        LOGGER.error("Unable to publish message to kafka.", e);
        success = false;
      } else
        LOGGER.debug("The offset of the record we just sent is: " + recordMetadata.offset());
    }
  }

  /**
   * main.
   * 
   * @param args main arguments
   * @throws Exception when this exceptional condition happens
   */
  public static void main(String... args) throws Exception {
    String jarFileName = new java.io.File(
        MainProducer.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();

    if (args.length == 5) {
      LOGGER.info("servers: " + args[0]);
      LOGGER.info("topics: " + args[1]);
      LOGGER.info("num msg send sec: " + Integer.valueOf(args[2]));
      LOGGER.info("send delay (millisecond): " + Integer.valueOf(args[3]));
      LOGGER.info("Input File: " + args[4]);

      partitions = partitionFile(args[4], Integer.valueOf(args[2]));

      producer.createProducer(args[0]);

      MyCallback callBack = new MyCallback();

      sendMessages(args[1], Integer.valueOf(args[3]), callBack);

    } else {
      LOGGER.error(
          "Usage: java -jar {} <servers> <topic> <num msg send sec (integer)> <send delay (millisecond)> <input file>\n",
          jarFileName);
      LOGGER.error(
          "\tE.g.: java -jar {} 127.0.0.1:9092,127.0.0.1:9093,127.0.0.1:9094 test1 10 5000 /data/data_to_send.csv \n",
          jarFileName);
    }

  }

  /**
   * sendMessages.
   * 
   * @param topic topic lists
   * @param delay delay number
   */
  public static void sendMessages(String topic, Integer delay, final Callback callBack) {
    Timer t = new Timer();
    t.schedule(new TimerTask() {
      @Override
      public void run() {

        try {
          LOGGER.info("partiotionElaboratedPosition: " + partiotionElaboratedPosition);
          LOGGER.info("partitions.size(): " + partitions.size());

          List<String> list = partitions.get(partiotionElaboratedPosition);
          for (String row : list) {
            producer.sendMessage(row, topic, callBack);
          }
          partiotionElaboratedPosition++;

          if (partiotionElaboratedPosition == partitions.size()) {
            producer.flush();
            producer.closeProducerSession();
            t.cancel();
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }, 0, delay);
  }

  /**
   * 
   * @param fileName filename contains all the messages
   * @param partitionSize the size which you want use to partitioning the file
   * @return a List of List
   * @throws Exception when this exceptional condition happens
   */
  private static List<List<String>> partitionFile(String fileName, int partitionSize)
      throws Exception {
    File f = new File(fileName);

    List<String> lista = new ArrayList<String>();

    try (BufferedReader br = new BufferedReader(new FileReader(f))) {
      for (String line; (line = br.readLine()) != null;) {
        lista.add(line);
      }
    }

    List<List<String>> partitions = new ArrayList<>();

    for (int i = 0; i < lista.size(); i += partitionSize) {
      partitions.add(lista.subList(i, Math.min(i + partitionSize, lista.size())));
    }
    return partitions;
  }
}
