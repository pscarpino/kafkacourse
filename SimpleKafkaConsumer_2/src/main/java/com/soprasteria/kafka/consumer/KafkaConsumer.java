package com.soprasteria.kafka.consumer;

import java.util.Collections;
import java.util.Properties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author paoloscarpino
 *
 */
public class KafkaConsumer {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

  private Consumer<String, String> consumer;
  private boolean isStopped;

  /**
   * 
   * @param groupId
   * @param servers
   * @param topics
   */
  public KafkaConsumer(String groupId, String servers, String topics) {
    super();
    createConsumer(groupId, servers, topics);
  }

  /**
   * 
   * @param groupId
   * @param servers
   * @param topics
   */
  private void createConsumer(final String groupId, final String servers, final String topics) {
    final Properties props = new Properties();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

    // Create the consumer using props.
    consumer = new org.apache.kafka.clients.consumer.KafkaConsumer<>(props);

    // Subscribe to the topic.
    consumer.subscribe(Collections.singletonList(topics));

    isStopped = false;
  }

  /**
   * 
   * @throws Exception
   */
  public void runConsumer() throws Exception {
    try {
      while (!isStopped) {
        final ConsumerRecords<String, String> consumerRecords = consumer.poll(100);

        for (ConsumerRecord<String, String> record : consumerRecords) {
          LOGGER.info(record.topic() + "#" + record.offset() + "#" + record.value());
        }

        consumer.commitSync();
      }
    } finally {
      consumer.close();
    }
  }

  /**
   * 
   */
  public void stopConsumer() {
    isStopped = true;
  }
}
