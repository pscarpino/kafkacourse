package com.soprasteria.kafka.producer;

import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * java -jar
 * /Users/paoloscarpino/eclipse-workspace/SimpleKafkaProducer_1/target/SimpleKafkaProducer_1-1.0.0-
 * shaded.jar 127.0.0.1:9092 test1 "Hello World"
 */

/**
 * MainProducer.
 * 
 * @author paoloscarpino
 *
 */
public class MainProducer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MainProducer.class);

  /**
   * main.
   * 
   * @param args main arguments
   * @throws Exception when this exceptional condition happens
   */
  public static void main(String... args) throws Exception {

    Properties props = new Properties();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
    props.put(ProducerConfig.CLIENT_ID_CONFIG, "SimpleKafkaProducer_1");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

    Producer<String, String> producer = new KafkaProducer<>(props);


    try {

      long time = System.currentTimeMillis();

      /**
       * Create a record to be sent to Kafka
       * 
       * @param topic The topic the record will be appended to
       * @param key The key that will be included in the record
       * @param value The record contents
       */
      final ProducerRecord<String, String> record = new ProducerRecord<>("TOPIC_TEST_2", "Key_" + time, "Hi There!!!");

      /**
       * Send the given record asynchronously and return a future which will eventually contain the
       * response information. Get ---> Waits if necessary for the computation to complete, and then
       * retrieves its result.
       */
      RecordMetadata metadata = producer.send(record).get();

      long elapsedTime = System.currentTimeMillis() - time;
      LOGGER.info("sent record(key={} value={}) " + "meta(partition={}, offset={}) time={}\n",
          record.key(), record.value(), metadata.partition(), metadata.offset(), elapsedTime);

      producer.flush();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      producer.close();
    }

  }
}
