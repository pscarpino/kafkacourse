package com.soprasteria.kafka.producer;

import java.util.Properties;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * KafkaProducer.
 * 
 * @author paoloscarpino
 *
 */
public class KafkaProducer {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

  private Producer<String, String> producer;

  /**
   * createProducer.
   * 
   * @param servers brokers list
   */
  public void createProducer(final String servers) {
    Properties props = new Properties();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
    props.put(ProducerConfig.CLIENT_ID_CONFIG, "SimpleKafkaProducer_2");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    producer = new org.apache.kafka.clients.producer.KafkaProducer<>(props);
  }

  /**
   * sendMessage.
   * 
   * @param msg message to send
   * @param topic topic list
   * @throws Exception when this exceptional condition happens
   */
  public void sendMessage(final String msg, final String topic) throws Exception {
    long time = System.currentTimeMillis();

    try {
      final ProducerRecord<String, String> record = new ProducerRecord<>(topic, "" + time, msg);

      RecordMetadata metadata = producer.send(record).get();

      long elapsedTime = System.currentTimeMillis() - time;
      LOGGER.info("sent record(key={} value={}) " + "meta(partition={}, offset={}) time={}\n",
          record.key(), record.value(), metadata.partition(), metadata.offset(), elapsedTime);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * flush.
   */
  public void flush() {
    producer.flush();
  }

  /**
   * closeProducerSession.
   * 
   * @throws Exception when this exceptional condition happens
   */
  public void closeProducerSession() throws Exception {
    producer.close();
  }


}
