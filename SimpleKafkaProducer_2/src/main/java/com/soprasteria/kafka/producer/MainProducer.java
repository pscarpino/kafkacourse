package com.soprasteria.kafka.producer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * java -jar
 * /Users/paoloscarpino/eclipse-workspace/SimpleKafkaProducer_1/target/SimpleKafkaProducer_1-1.0.0-
 * shaded.jar 127.0.0.1:9092 test1 "Hello World"
 */

/**
 * MainProducer.
 * 
 * @author paoloscarpino
 *
 */
public class MainProducer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MainProducer.class);

  private static KafkaProducer producer = new KafkaProducer();

  /**
   * main.
   * 
   * @param args main arguments
   * @throws Exception when this exceptional condition happens
   */
  public static void main(String... args) throws Exception {
    String jarFileName = new java.io.File(
        MainProducer.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();

    if (args.length == 3) {
      LOGGER.info("servers: " + args[0]);
      LOGGER.info("topics: " + args[1]);
      LOGGER.info("Input File: " + args[2]);

      List<String> lista = loadFile(args[2]);

      producer.createProducer(args[0]);
      for (String row : lista) {
        producer.sendMessage(row, args[1]);
        producer.flush();
      }
      producer.closeProducerSession();

    } else {
      LOGGER.error("Usage: java -jar {} <servers> <topic> <Input File>\n", jarFileName);
      String err = String.format(
          "E.g.: java -jar %s 127.0.0.1:9092,127.0.0.1:9093,127.0.0.1:9094 test1 input_file.csv",
          jarFileName);

      LOGGER.error(err);
    }
  }

  /**
   * 
   * @param fileName filename contains all the messages
   * @return a List contains csv lines
   * @throws Exception when this exceptional condition happens
   */
  private static List<String> loadFile(String fileName) throws Exception {
    File f = new File(fileName);

    List<String> lista = new ArrayList<String>();

    try (BufferedReader br = new BufferedReader(new FileReader(f))) {
      for (String line; (line = br.readLine()) != null;) {
        lista.add(line);
      }
    }

    return lista;
  }

}
